
% clear all, close all

import_data = xlsread('C:\Users\simon\polybox\01_Balgrist\02_Research\01_Projects\04_Shoulder-Instability_Hochreiter\3D-Testing\Test-MEANProblem.xlsx');

a = import_data(:,1:2);
b = import_data(:,4:5);
c = import_data(:,7:8);

mat_mean = (a+b+c)/3;

a = [a(:,2) a(:,1)]; 
a(end-2:end,:) = [];
b = [b(:,2) b(:,1)]; 
b(end-2:end,:) = [];
c = [c(:,2) c(:,1)]; 
c(end-2:end,:) = [];



db_min = min([a(:,1);b(:,1);c(:,1)]);

d = linspace(0,db_min,1000);
aa = interp1(a(:,1)+0.000001*(1:size(a(:,1)))', a(:,2),d')
bb = interp1(b(:,1)+0.000001*(1:size(b(:,1)))', b(:,2),d')
cc = interp1(c(:,1)+0.000001*(1:size(c(:,1)))', c(:,2),d')

mat_mean2 = nanmean([aa,bb,cc],2);

aa = unique(a, 'row');
figure;
hold on;
plot(a(:,1), a(:,2))
plot(b(:,1), b(:,2))
plot(c(:,1), c(:,2))
plot(d, mat_mean2, 'Linewidth', 3)
legend('T1','T2','T3', 'mean')
set(gca, 'Fontsize',20)
xlabel('Strain [mm]')
ylabel('Standard Force [N]')

figure(2);
hold on;
plot(a(:,1), a(:,2))
plot(b(:,1), b(:,2))
plot(c(:,1), c(:,2))
plot(mat_mean(:,2), mat_mean(:,1), 'Linewidth', 3)
legend('T1','T2','T3', 'mean')
set(gca, 'Fontsize',20)
xlabel('Strain [mm]')
ylabel('Standard Force [N]')
