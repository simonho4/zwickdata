clear all,

data = []

for i = 4:6
import_data = xlsread('C:\Users\simon\polybox\01_Balgrist\02_Research\01_Projects\04_Shoulder-Instability_Hochreiter\3D-Testing\DiLillo-RawZwick.xls', i);
%import_data2 = readtable('C:\Users\simon\polybox\01_Balgrist\02_Research\01_Projects\04_Shoulder-Instability_Hochreiter\3D-Testing\DiLillo-RawZwick.xls', 'Sheet', i);
import_data = -1 * import_data(:,2:3);
force = import_data(:,1);
disp = import_data(:,2);

data = [data import_data]; %append
% fo = [fo force];
% di = [di disp];

end

t1 = data(:,1:2);
t2 = data(:,3:4);
t3 = data(:,5:6);

t1 = [t1(:,2) t1(:,1)]; % column 1 = displacement, col. 2 =force
t1(end-2:end,:) = [];
t2 = [t2(:,2) t2(:,1)]; 
t2(end-2:end,:) = [];
t3 = [t3(:,2) t3(:,1)]; 
t3(end-2:end,:) = [];

mat_mean1 = (t1+t2+t3)/3;

db_max = max([t1(:,1);t2(:,1);t3(:,1)]);

d = linspace(0,db_max,10000);
aa = interp1(t1(:,1)+0.000001*(1:size(t1(:,1)))', t1(:,2),d')
bb = interp1(t2(:,1)+0.000001*(1:size(t2(:,1)))', t2(:,2),d')
cc = interp1(t3(:,1)+0.000001*(1:size(t3(:,1)))', t3(:,2),d')

mat_mean = nanmean([aa,bb,cc],2);

%aa = unique(t1, 'row');

figure;
hold on;
plot(t1(:,1), t1(:,2))
plot(t2(:,1), t2(:,2))
plot(t3(:,1), t3(:,2))
plot(d, mat_mean, 'Linewidth', 3)
legend('T1','T2','T3', 'mean', 'Location', 'southoutside', 'Orientation', 'horizontal')
set(gca, 'Fontsize',20)
set(gcf,  'Position', [320,285,1258,643])
xlabel('Strain [mm]')
ylabel('Standard Force [N]')
hold off

figure(2);
hold on;
plot(t1(:,1), t1(:,2))
plot(t2(:,1), t2(:,2))
plot(t3(:,1), t3(:,2))
plot(mat_mean1(:,1), mat_mean1(:,2), 'Linewidth', 3)
legend('T1','T2','T3', 'mean', 'Location', 'southoutside', 'Orientation', 'horizontal')
set(gca, 'Fontsize',20)
set(gcf,  'Position', [320,285,1258,643])
xlabel('Strain [mm]')
ylabel('Standard Force [N]')
hold off

