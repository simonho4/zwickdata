
clear all, close all
 %% import data
% loop to import data, store data in 3D matrix using another loop, comment to indicate what is
% what.


    for s = 1:54
import_data = xlsread('C:\Users\simon\polybox\01_Balgrist\02_Research\01_Projects\04_Shoulder-Instability_Hochreiter\3D-Testing\01_Final_Data_100N.xls', s);

% change i to match final data excel
dat(:,:,s) = -1 * import_data(:,2:3);
force = import_data(:,1);
disp = import_data(:,2);


% fo = [fo force];
% di = [di disp];
    end
    
t1 = []
t2 = []
t3 = []

%% process data
load ('C:\Users\simon\polybox\01_Balgrist\02_Research\01_Projects\04_Shoulder-Instability_Hochreiter\3D-Testing\Vector-Tekscan\data-3dmatrix.mat');
j = 1
    for i = 1:3:54

t1 = dat(:,:,i); % test 1
t2 = dat(:,:,i+1);
t3 = dat(:,:,i+2);

t1 = [t1(:,2) t1(:,1)]; % column 1 = displacement, col. 2 =force
t1(end-2:end,:) = [];
t2 = [t2(:,2) t2(:,1)]; 
t2(end-2:end,:) = [];
t3 = [t3(:,2) t3(:,1)]; 
t3(end-2:end,:) = [];

mat_mean1 = (t1+t2+t3)/3;

% remove data of machine going back to start position. Find max
% displacement and remove data from there

[d_max1,idm1] = max(t1(:,1));
[d_max2, idm2] = max(t2(:,1));
[d_max3, idm3] = max(t3(:,1));

t1(idm1:end,:) = [];
t2(idm2:end,:) = [];
t3(idm3:end,:) = [];

db_max = max([t1(:,1);t2(:,1);t3(:,1)]);

d = linspace(0,db_max,10000)'; % goes to column 3 of 3D matrix
aa = interp1(t1(:,1)+0.000001*(1:size(t1(:,1)))', t1(:,2),d); %force interpolation
bb = interp1(t2(:,1)+0.000001*(1:size(t2(:,1)))', t2(:,2),d);
cc = interp1(t3(:,1)+0.000001*(1:size(t3(:,1)))', t3(:,2),d);


mat_mean = nanmean([aa,bb,cc],2); %mean of 3 tests (interpolated)

d_f = [d mat_mean]

means(:,:,j)= d_f; %

% 
% figure();
% hold on;
% plot(t1(:,1), t1(:,2))
% plot(t2(:,1), t2(:,2))
% plot(t3(:,1), t3(:,2))
% plot(means(:,1,j), means(:,2,j), 'Linewidth', 3)
% legend('T1','T2','T3', 'mean', 'Location', 'southoutside', 'Orientation', 'horizontal')
% set(gca, 'Fontsize',20)
% set(gcf,  'Position', [320,285,1258,643])
% xlabel('Strain [mm]')
% ylabel('Standard Force [N]')
% hold off

j = j + 1
%         end
    end


%% make plots by degree

d35 = means (:,:,1:6); % order is SSM, B1, B1 Acromion corr., B1 Glenoid Corr, B1 Ac& Gl, 
d60 = means (:,:,7:12);
d75 = means (:,:,13:18);

%35 Deg Glenohumeral
figure();
hold on;
for ii = 1: size(d35,3)

plot(d35(:,1,ii), d35(:,2,ii), 'Linewidth', 3)
lgd =legend('SSM','B1','B1 Acromion Corrected', 'B1 Glenoid Corrected', 'Acromion & Glenoid Corrected', 'Di Lillo', 'Location', 'southoutside','Orientation', 'horizontal');
lgd.NumColumns = 3
set(gca, 'Fontsize',20)
set(gcf,  'Position', [320,285,1258,643])
xlabel('Strain [mm]')
ylabel('Standard Force [N]')
title (' 35° Means')

end
hold off

figure();
hold on;
for ii = 1: size(d60,3)

plot(d60(:,1,ii), d60(:,2,ii), 'Linewidth', 3)
lgd =legend('SSM','B1','B1 Acromion Corrected', 'B1 Glenoid Corrected', 'Acromion & Glenoid Corrected', 'Di Lillo', 'Location', 'southoutside','Orientation', 'horizontal');
lgd.NumColumns = 3
set(gca, 'Fontsize',20)
set(gcf,  'Position', [320,285,1258,643])
xlabel('Strain [mm]')
ylabel('Standard Force [N]')
title ('60° Means')

end
hold off

figure();
hold on;
for ii = 1: size(d60,3)

plot(d75(:,1,ii), d75(:,2,ii), 'Linewidth', 3)
lgd =legend('SSM','B1','B1 Acromion Corrected', 'B1 Glenoid Corrected', 'Acromion & Glenoid Corrected', 'Di Lillo', 'Location', 'southoutside','Orientation', 'horizontal');
lgd.NumColumns = 3
set(gca, 'Fontsize',20)
set(gcf,  'Position', [320,285,1258,643])
xlabel('Strain [mm]')
ylabel('Standard Force[N]')
title ('75° Means')

end

